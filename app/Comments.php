<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    //
    protected $fillable = ['content', 'post_id', 'id'];

    protected $prymaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected  static function boot()

    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->id)) {
                $model->id = Str::uuid();
            }
        });
    }

    public function posts()
    {
        return $this->belongsTo('App\posts');
    }
}
