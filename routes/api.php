<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('/post', 'PostController');
Route::apiResource('/roles', 'RolesController');
Route::apiResource('/comments', 'CommentsController');

Route::group(['previx' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('auth/register', 'RegisterController')->name('auth.register');
});
